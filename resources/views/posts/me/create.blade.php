@extends("layouts.app")

@section("content")
    <main class="col-md-10">
        <div class="container">
            <form action="{{ route("posts.me.store") }}" METHOD="POST" enctype="multipart/form-data" onsubmit="createPostForm(event)" id="post-form">
                @csrf
                <div class="form-group">
                    <label>Заголовок Поста</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Авторы</label>
                    <select class="form-control" name="author">
                        @foreach($authors as $author)
                            <option value="{{ $author->id }}">{{ $author->full_name }}</option>
                        @endforeach
                    </select>
                </div>



                {{--                <div id="categories-for-post" class="row col-sm-10"></div>--}}
                {{--                <div class="form-group">--}}
                {{--                    <label for="">Категории</label>--}}
                {{--                    <input type="text" list="categories-post-list"--}}
                {{--                           name="categories" class="form-control"--}}
                {{--                           onfocus="getCategories()"--}}
                {{--                           onfocusout="deleteOptionsAfterFocusOut('categories')"--}}
                {{--                           onkeyup="addNewCategoryForPost(event)"--}}
                {{--                           id="post-categories" placeholder="" autocomplete="off">--}}
                {{--                    <datalist id="categories-post-list">--}}
                {{--                        <select multiple class="form-control" id="categories-post-select"></select>--}}
                {{--                    </datalist>--}}
                {{--                    <input hidden type="text" name="all-tags-to-post" id="all-categories-to-post" multiple>--}}
                {{--                </div>--}}


                @include("forms.form-tags")

                <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="images[]" multiple class="form-control-file">
                </div>

                <div class="form-group">
                    <label>Описание</label>
                    <textarea class="form-control" name="description" rows="3"></textarea>
                </div>

                <button class="btn btn-secondary" onclick="document.getElementById('post-form').submit()">Save</button>
            </form>
        </div>
    </main>
@endsection
