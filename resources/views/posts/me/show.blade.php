@extends("layouts.personal")

@section("content")
    <main class="post blog-post col-lg-10">
        <div class="container">
            <div class="post-single">
                <div class="post-thumbnail"><img src="{{ asset("storage/images/{$post->images->first()->name}") ?? "" }}" alt="..." class="img-fluid"></div>
                <div class="post-details">
                    <div class="post-meta d-flex justify-content-between">
                        <div class="category"><a href="#">Business</a><a
                                href="#">{{ $post->categories->first()->name ?? "" }}</a></div>
                    </div>
                    <h1>{{ $post->title }}<a href="#"><i
                                class="fa fa-bookmark-o"></i></a></h1>
                    <div class="post-footer d-flex align-items-center flex-column flex-sm-row"><a href="#"
                                                                                                  class="author d-flex align-items-center flex-wrap">
                            <div class="avatar"><img src="img/avatar-1.jpg" alt="..." class="img-fluid"></div>
                            <div class="title"><span>{{ $post->authors->full_name }}</span></div>
                        </a>
                        <div class="d-flex align-items-center flex-wrap">
                            <div class="date"><i class="icon-clock"></i> 2 months ago</div>
                            <div class="views"><i class="icon-eye"></i> 500</div>
                            <div class="comments meta-last"><i class="icon-comment"></i>12</div>
                        </div>
                    </div>
                    <div class="post-body">
                        {!! $post->description !!}
                    </div>
                    <div class="post-tags">
                        @foreach($post->tags as $postTag)
                            <a href="#" class="tag">#{{ $postTag->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
