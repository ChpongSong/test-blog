@extends("layouts.app")

@section("content")
    <main class="post blog-post col-lg-8">
        <div class="container">
            <div class="row">
            @foreach($posts as $post)
                <!-- post -->
                    <div class="post col-xl-6">
                        <div class="post-thumbnail"><a href="#"><img src="
                        @if($post->images->first())
                                {{ asset("storage/images/{$post->images->first()->name}") ?? "" }}"
                                                                     @endif
                                                                     alt="..." class="img-fluid"></a></div>
                        <div class="post-details">
                            <div class="post-meta d-flex justify-content-between">
                                <div class="date meta-last">20 May | 2016</div>
                                <div class="category"><a href="#">Business</a></div>
                            </div>
                            <a href="{{ url("posts.show", ["slug" => $post->post_slug]) }}">
                                <h3 class="h4">{{ $post->title }}</h3></a>
                            <p class="text-muted">
                                {{ Str::limit($post->description, 50) }}
                            </p>
                            <footer class="post-footer d-flex align-items-center"><a href="#"
                                                                                     class="author d-flex align-items-center flex-wrap">
                                    <div class="avatar"><img src="img/avatar-{{ rand(1, 3) }}.jpg" alt="..."
                                                             class="img-fluid"></div>
                                    <div class="title"><span>{{ $post->authors->first()->full_name ?? "" }}</span></div>
                                </a>
                                <div class="date"><i class="icon-clock"></i> 2 months ago</div>
                                <div class="comments meta-last"><i class="icon-comment"></i>12</div>
                            </footer>
                        </div>
                    </div>
                @endforeach

            </div>

            <!-- Pagination -->
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-template d-flex justify-content-center">
                    <li class="page-item"><a href="{{ $posts->previousPageUrl() }}" class="page-link">
                            <i class="fa fa-angle-left"></i></a></li>
                    <li class="page-item"><a href="{{ $posts->nextPageUrl() }}" class="page-link">
                            <i class="fa fa-angle-right"></i></a></li>
                </ul>
            </nav>
        </div>
    </main>
@endsection
@section("aside-bar")
    <aside class="col-lg-4">
        @include("widgets.search-bar")
        @include("widgets.latest-post", ["latestPosts" => $latestPosts])
        @include("widgets.popular-categories", ["categories" => $categories])
        @include("widgets.post-tags", ["tags" => $tags])
    </aside>
@endsection
