@extends("layouts.personal")

@section("content")
    <main class="col-sm-10">
        <div class="container">
            <form action="{{ route("user.store") }}" METHOD="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="{{ $user->name }}"
                           placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label>Avatar</label>
                    @if($user->avatars)
                        <img src="{{ asset("storage/images/{$user->avatars->name}") ?? "" }}"
                             class="img-thumbnail" alt="Cinque Terre">
                    @endif
                    <input type="file" name="avatar"
                           class="form-control-file">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </main>
@endsection
