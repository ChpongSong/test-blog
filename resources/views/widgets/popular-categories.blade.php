<!-- Widget [Categories Widget]-->
<div class="widget categories">
    <header>
        <h3 class="h6">Categories</h3>
    </header>
    @foreach($categories as $category)
        <div class="item d-flex justify-content-between"><a href="categories/{{ $category->category_slug }}">
                {{ $category->name }}
            </a><span>{{ rand(1, 100) }}</span></div>
    @endforeach
</div>
