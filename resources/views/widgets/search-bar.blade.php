<!-- Widget [Search Bar Widget]-->
<div class="widget search">
    <header>
        <h3 class="h6">Поиск</h3>
    </header>
    <form action="#" class="search-form" onsubmit="event.preventDefault()">
        <div class="form-group">
            <input type="search" placeholder="Поиск постов по тегам"
                   id="input-search" list="search-list" autocomplete="off"
            >
            <datalist id="search-list"></datalist>
            <button type="button" class="submit" id="search-btn-submit"><i class="icon-search"></i></button>
        </div>
    </form>
</div>
