{{--<div id="tags-for-post" class="row col-sm-10"></div>--}}
{{--<div class="form-group">--}}
{{--    <label for="">Теги</label>--}}
{{--    <input type="text" list="post-tags-list"--}}
{{--           onfocus="getTags()"--}}
{{--           onfocusout="deleteOptionsAfterFocusOut('tag')"--}}
{{--           onkeyup="addNewTagForPost(event)"--}}
{{--           name="tags" class="form-control"--}}
{{--           id="post-tags" placeholder="" autocomplete="off">--}}
{{--    <datalist id="post-tags-list">--}}
{{--        <select multiple class="form-control" id="post-tag-select"></select>--}}
{{--    </datalist>--}}
{{--    <input hidden type="text" name="all-tags-to-post" id="all-tags-to-post" multiple>--}}
{{--</div>--}}

<div class="form-group">
    <label>Теги</label>
    <select class="form-control" name="tags[]" multiple>
        @foreach($tags as $tag)
            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
</div>
