/*global $, document, Chart, LINECHART, data, options, window, setTimeout*/
$(document).ready(function () {

    'use strict';

    // ------------------------------------------------------- //
    // For demo purposes only
    // ------------------------------------------------------ //

    var stylesheet = $('link#theme-stylesheet');
    $("<link id='new-stylesheet' rel='stylesheet'>").insertAfter(stylesheet);
    var alternateColour = $('link#new-stylesheet');

    // if ($.cookie("theme_csspath")) {
    //     alternateColour.attr("href", $.cookie("theme_csspath"));
    // }

    $("#colour").change(function () {

        if ($(this).val() !== '') {

            var theme_csspath = 'css/style.' + $(this).val() + '.css';

            alternateColour.attr("href", theme_csspath);

            $.cookie("theme_csspath", theme_csspath, {
                expires: 365,
                path: document.URL.substr(0, document.URL.lastIndexOf('/'))
            });

        }

        return false;
    });


    // ------------------------------------------------------- //
    // Equalixe height
    // ------------------------------------------------------ //
    function equalizeHeight(x, y) {
        var textHeight = $(x).height();
        $(y).css('min-height', textHeight);
    }

    equalizeHeight('.featured-posts .text', '.featured-posts .image');

    $(window).resize(function () {
        equalizeHeight('.featured-posts .text', '.featured-posts .image');
    });


    // ---------------------------------------------- //
    // Preventing URL update on navigation link click
    // ---------------------------------------------- //
    $('.link-scroll').bind('click', function (e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top + 2
        }, 700);
        e.preventDefault();
    });


    // ---------------------------------------------- //
    // FancyBox
    // ---------------------------------------------- //
    // $("[data-fancybox]").fancybox();


    // ---------------------------------------------- //
    // Divider Section Parallax Background
    // ---------------------------------------------- //
    $(window).on('scroll', function () {

        var scroll = $(this).scrollTop();

        if ($(window).width() > 1250) {
            $('section.divider').css({
                'background-position': 'left -' + scroll / 8 + 'px'
            });
        } else {
            $('section.divider').css({
                'background-position': 'center bottom'
            });
        }
    });


    // ---------------------------------------------- //
    // Search Bar
    // ---------------------------------------------- //
    $('.search-btn').on('click', function (e) {
        e.preventDefault();
        $('.search-area').fadeIn();
    });
    $('.search-area .close-btn').on('click', function () {
        $('.search-area').fadeOut();
    });


    // ---------------------------------------------- //
    // Navbar Toggle Button
    // ---------------------------------------------- //
    $('.navbar-toggler').on('click', function () {
        $('.navbar-toggler').toggleClass('active');
    });

    $(".dropdown-toggle").dropdown();
});

const tagSelect = document.getElementById("post-tag-select");
const categoriesSelect = document.getElementById("categories-post-select");
const postsTagList = document.getElementById("post-tags-list");
const categoriesForPost = document.getElementById("categories-post-list");
var allTagsForPost = [];
var allCategoriesForPost = [];

async function getTags() {
    const res = await axios.post("http://127.0.0.1:8000/posts/tags/getTags");
    for (let tag of await res.data) {
        const option = document.createElement("option");
        option.text = tag.name;
        tagSelect.appendChild(option)
    }
}

async function getCategories() {
    const res = await axios.post("http://127.0.0.1:8000/posts/categories/getCategories");
    for (let category of await res.data) {
        const option = document.createElement("option")
        option.text = category.name;
        allCategoriesForPost.push(category.name);
        categoriesSelect.appendChild(option);
    }
}

// async function addNewCategoryForPost(event) {
//     const newCategoryValue = event.target.value;
//     if (event.keyCode === 13 && newCategoryValue.trim() !== "") {
//         // if (allCategoriesForPost.includes(newCategoryValue)) {
//         const newCategory = document.createElement("div");
//         newCategory.textContent = newCategoryValue;
//         newCategory.setAttribute("data-category-for-post", newCategoryValue);
//         newCategory.classList.add("btn-secondary")
//         newCategory.classList.add("col-sm-2")
//         newCategory.classList.add("post-btn")
//
//         newCategory.appendChild(createDeleteCategoryBtn(event));
//         console.log(newCategory);
//         event.target.value = "";
//         document.getElementById("categories-for-post").appendChild(newCategory);
//         // }
//     }
// }

async function addNewTagForPost(event) {
    let newPostValue = event.target.value;
    if (event.keyCode === 13 && newPostValue.trim() !== "") {
        let newTag = document.createElement("div");
        newTag.textContent = newPostValue;
        newTag.setAttribute("data-post-tag", newPostValue);
        newTag.classList.add("btn-secondary")
        newTag.classList.add("col-sm-2")
        newTag.classList.add("post-btn")

        newTag.appendChild(createDeleteTageBtn(event));
        allTagsForPost.push(newPostValue);
        event.target.value = "";
        console.log(document.getElementById("tags-for-post"))
        document.getElementById("tags-for-post").appendChild(newTag);
    }
}

function createDeleteCategoryBtn(event) {
    const deleteCategoryBtn = document.createElement("button");
    deleteCategoryBtn.classList.add("btn");
    deleteCategoryBtn.classList.add("fa");
    deleteCategoryBtn.classList.add("fa-trash");
    deleteCategoryBtn.addEventListener("click", async function (event) {
        event.target.parentElement.remove();
    })

    return deleteCategoryBtn;
}

function createDeleteTageBtn(event) {
    const deleteTagBtn = document.createElement("button");
    deleteTagBtn.classList.add("btn");
    deleteTagBtn.classList.add("fa");
    deleteTagBtn.classList.add("fa-trash");
    deleteTagBtn.addEventListener("click", async function (event) {
        const postTag = event.target.parentElement.getAttribute("data-post-tag");
        allTagsForPost = allTagsForPost.filter(el => el !== postTag);
        event.target.parentElement.remove();
    })

    return deleteTagBtn;
}


async function deleteOptionsAfterFocusOut(token) {

    if (token === "tag") {
        tagSelect.querySelectorAll("*").forEach(node => node.remove());
    } else if (token === "categories") {
        categoriesSelect.querySelectorAll("*").forEach(node => node.remove());
    }
}

function createPostForm(event) {
    event.preventDefault();
}

const inputSearch = document.getElementById("input-search");
const searchDataList = document.getElementById("search-list");
let timeout = null;

inputSearch.addEventListener("keypress", async function (event) {
    if (event.keyCode === 13) {
        const tag = event.target.value;
        window.location.href = `http://127.0.0.1:8000/posts/search?tag=${tag.replace("#", '')}`;
    }
});

inputSearch.addEventListener("keyup", async function (event) {
    clearTimeout(timeout);
    timeout = setTimeout(async (keyBoardEvent) => {
        const res = await axios.post("http://127.0.0.1:8000/posts/search/searchByTagName", {
            tagName: keyBoardEvent.target.value,
        });
        searchDataList.querySelectorAll("*").forEach(node => node.remove());
        for (let tag of await res.data.tags) {
            let option = document.createElement("option");
            option.value = '#' + tag.name;
            searchDataList.appendChild(option)
        }
    }, 1000, (event))
});


inputSearch.addEventListener("focus", async function (event) {

    const res = await axios.post("http://127.0.0.1:8000/posts/search/getPopularTags")

    for (let tag of await res.data) {
        let option = document.createElement("option");
        option.value = '#' + tag.name;
        searchDataList.appendChild(option)
    }
});

inputSearch.addEventListener("focusout", function (event) {
    searchDataList.querySelectorAll("*").forEach(node => node.remove());
});
