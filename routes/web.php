<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get("", function () {
    if (\Illuminate\Support\Facades\Auth::check()) {
        return redirect()->route("posts.index");
    }
    return redirect()->route("login");
});
Route::get('/home', 'HomeController@index')->name('home');

Route::group(["prefix" => "posts", "namespace" => "Posts"], function () {
    Route::get("", "PostController@index")->name("posts.index");
    Route::get("show/{slug}", "PostController@show")->name("posts.show");

    Route::group(["prefix" => "search"], function () {
        Route::post("getPopularTags", "SearchController@getPopularTags");
        Route::post("searchByTagName", "SearchController@searchPostByTagName");
        Route::get("", "SearchController@getPostsByTag");
    });

    Route::group(["prefix" => "me"], function () {
        Route::get("", "PostController@getPostByUserId")->name("posts.me.index");
        Route::get("create", "PostController@create")->name("posts.me.create");
        Route::post("store", "PostController@store")->name("posts.me.store");
        Route::get("show/{slug}", "PostController@showPostForOwner")->name("posts.me.show");
    });

    Route::group(["prefix" => "tags"], function () {
        Route::post("getTags", "TagController@getPopularTags");
    });
    Route::group(["prefix" => "categories"], function () {
        Route::post("getCategories", "CategoryController@getPopularCategories");
    });
});

Route::group(["prefix" => "user", "namespace" => "Posts"], function () {
   Route::get("show", "UserController@show")->name("user.show");
   Route::post("store", "UserController@store")->name("user.store");
});
