<?php


namespace App\DTO\posts;


use DateTime;

class PostCreateDto
{
    /**
     * @var int
     */
    private int $authorId;
    /**
     * @var int
     */
    private int $user_id;
    /**
     * @var string
     */
    private string $title;
    /**
     * @var string
     */
    private string $description;
    /**
     * @var string
     */
    private string $slug;

    /**
     * @var DateTime
     */
    private DateTime $published_at;
    /**
     * @var bool
     */
    private bool $is_published;


    /**
     * PostCreateDto constructor.
     * @param int $authorId
     * @param int $user_id
     * @param string $title
     * @param string $description
     * @param string $slug
     * @param DateTime $published_at
     * @param bool $is_published
     */
    public function __construct
    (
        int $authorId,
        int $user_id,
        string $title,
        string $description,
        string $slug,
        DateTime $published_at,
        bool $is_published
    )
    {
        $this->authorId = $authorId;
        $this->user_id = $user_id;
        $this->title = $title;
        $this->description = $description;
        $this->slug = $slug;
        $this->published_at = $published_at;
        $this->is_published = $is_published;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @param int $authorId
     */
    public function setAuthorId(int $authorId): void
    {
        $this->authorId = $authorId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return DateTime
     */
    public function getPublishedAt(): DateTime
    {
        return $this->published_at;
    }

    /**
     * @param DateTime $published_at
     */
    public function setPublishedAt(DateTime $published_at): void
    {
        $this->published_at = $published_at;
    }

    /**
     * @return bool
     */
    public function getIsPublished(): bool
    {
        return $this->is_published;
    }

    /**
     * @param bool $is_published
     */
    public function setIsPublished(bool $is_published): void
    {
        $this->is_published = $is_published;
    }

}
