<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\PostTags
 *
 * @property int $id
 * @property int $tag_id
 * @property int $post_id
 * @method static Builder|PostTags newModelQuery()
 * @method static Builder|PostTags newQuery()
 * @method static Builder|PostTags query()
 * @method static Builder|PostTags whereId($value)
 * @method static Builder|PostTags wherePostId($value)
 * @method static Builder|PostTags whereTagId($value)
 * @mixin Eloquent
 */
class PostTags extends Pivot
{
    public $table = "post_tags";
}
