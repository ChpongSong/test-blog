<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\PostCategory
 *
 * @property int $id
 * @property int $category_id
 * @property int $post_id
 * @method static Builder|PostCategory newModelQuery()
 * @method static Builder|PostCategory newQuery()
 * @method static Builder|PostCategory query()
 * @method static Builder|PostCategory whereCategoryId($value)
 * @method static Builder|PostCategory whereId($value)
 * @method static Builder|PostCategory wherePostId($value)
 * @mixin \Eloquent
 */
class PostCategory extends Pivot
{
    protected $guarded = [];

    public $table = "post_categories";
}
