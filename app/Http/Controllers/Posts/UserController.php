<?php

namespace App\Http\Controllers\Posts;

use App\Contracts\Service;
use App\DTO\users\UserCreateDto;
use App\Http\Controllers\Controller;
use App\Services\Posts\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    private Service $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function show()
    {

        return view("posts.profile.show", [
            "user" => Auth::user()
        ]);
    }

    public function store(Request $request)
    {

        $dto = new UserCreateDto(
            $request->get("name"),
            $request->get("email") ?? Auth::user()->email,
            $request->get("password") ?? Auth::user()->password
        );

        if (!$this->userService->update($dto)) {
            return redirect()->back();
        }

//        if ($request->hasFile("avatar")) {
//            $file = $request->file("avatar");
//            $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();
//            $filePath = $file->storeAs('public/images', $fileName);
//            Image::query()->create([
//                "name" => $fileName,
//                "imageable_id" => Auth::id(),
//                "imageable_type" => User::class
//            ]);
//            return back();
//        }
    }
}
