<?php

namespace App\Http\Controllers\Posts;

use App\Contracts\Service;
use App\DTO\posts\PostCreateDto;
use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Services\Posts\AuthorService;
use App\Services\Posts\CategoryService;
use App\Services\Posts\PostService;
use App\Services\Posts\TagService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\View\View;

class PostController extends Controller
{

    private Service $postService, $categoryService, $tagService, $authorService;

    /**
     * PostController constructor.
     * @param PostService $postService
     * @param CategoryService $categoryService
     * @param TagService $tagService
     * @param AuthorService $authorService
     */
    public function __construct(
        PostService $postService,
        CategoryService $categoryService,
        TagService $tagService,
        AuthorService $authorService
    )
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
        $this->tagService = $tagService;
        $this->authorService = $authorService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {

        return view("posts.index", [
            "tags" => $this->tagService->getPopular(5),
            "categories" => $this->categoryService->getPopular(5),
            "posts" => $this->postService->getAll(10),
            "latestPosts" => $this->postService->getLastPosts()
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function show()
    {
        return view("posts.show", [
            "tags" => $this->tagService->getAll(10),
            "categories" => $this->categoryService->getAll(5)
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view("posts.me.create", [
            "authors" => $this->authorService->getAll(),
            "categories" => $this->categoryService->getAll(),
            "tags" => $this->tagService->getAll()
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {

        $postDto = new PostCreateDto(
            $request->get("author"),
            $request->user()->id,
            $request->get("title"),
            $request->get("description"),
            Str::slug($request->get('title')),
            now(),
            1
        );
        $tags = $request->get("tags");
        $images = $request->hasFile("images") ? $request->file("images") : false;

        if (!$this->postService->create($postDto, $tags, $images)) {
            return redirect()->route("posts.me.index");
        }
        return back()->withErrors("");
    }

    public function edit()
    {
        $authors = Author::query()->get();
        $categories = Category::query()->get();
        $tags = Tag::query()->get();

        return view("posts.me.create", [
            "authors" => $authors,
            "categories" => $categories,
            "tags" => $tags
        ]);
    }


    public function getPostByUserId()
    {
        $posts = Post::query()->where("user_id", Auth::id())
            ->with(["images", "authors"])
            ->get();
        return view("posts.me.index", compact("posts"));
    }

    public function showPostForOwner($slug)
    {
        $post = Post::query()->where(["user_id" => Auth::id(), "post_slug" => $slug])->first();
        return view("posts.me.show", [
            "post" => $post
        ]);
    }
}
