<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function getPopularTags()
    {
        return Tag::query()->limit(10)->get();
    }

    public function searchPostByTagName(Request $request)
    {
        $tagName = $request->get("tagName");
        $tags = Tag::query()->select("name")->where("name", 'ilike', "%{$tagName}%")->get();
        $tags->each(function ($tag) {
            return $tag;
        });

        return response([
            "tags" => $tags->toArray()
        ])->setStatusCode(200);
    }

    public function getPostsByTag(Request $request)
    {
        if ($request->has("tag")) {
            $tagName = $request->get("tag");
            $posts = Post::query()
                ->orderBy("published_at", "desc")
                ->where("is_published", 1)
                ->whereHas("tags", function ($query) use ($tagName) {
                    $query->where('name', $tagName);
            })->get();

            return view("posts.search.post-on-tag", [
//                "tags" => $tags,
//                "categories" => $categories,
                "posts" => $posts
            ]);
        }
    }
}
