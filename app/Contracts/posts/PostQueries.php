<?php


namespace App\Contracts\posts;

use App\Models\Post;

interface PostQueries
{
    public function getAll(int $perPage = 0);
    public function getById($id): Post;
    public function getLastPosts();
    public function getTopPosts();
    public function getUserPosts(int $userId);
}
