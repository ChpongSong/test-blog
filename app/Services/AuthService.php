<?php


namespace App\Services;


use App\Contracts\Service;

final class AuthService extends BaseService
{
    private Service $userService;

    public function __construct(Service $UserService)
    {
        $this->userService = $UserService;
    }
}
