<?php


namespace App\Services\Posts;


use App\DTO\users\UserCreateDto;
use App\Services\BaseService;
use App\User;

final class UserService extends BaseService
{
    public function create(UserCreateDto $userDto)
    {}

    /**
     * @param UserCreateDto $userDto
     * @return bool
     */
    public function store(UserCreateDto $userDto): bool
    {
        $user = new User();
        $user->name = $userDto->getName();
        $user->email = $userDto->getEmail();
        $user->password = $userDto->getPassword();
        if (!$user->save()) {
            return false;
        }

        return true;
    }

    /**
     * @param UserCreateDto $userDto
     * @return bool
     */
    public function update(UserCreateDto $userDto): bool
    {
        if (!User::query()->where("name", $userDto->getEmail())->first()) {
            return false;
        }

        return true;
    }
}
