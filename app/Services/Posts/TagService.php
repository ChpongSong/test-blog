<?php


namespace App\Services\Posts;

use App\Models\Tag;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection as SupCollection;

class TagService extends BaseService
{

    /**
     * @param int $limit
     * @return Tag[]|Builder[]|Collection|QueryBuilder[]|SupCollection
     */
    public function getAll(int $limit = 0)
    {
        if ($limit > 0) {
            return Tag::query()->limit($limit)->get();
        }

        return Tag::query()->get();
    }

    /**
     * @param int $limit
     * @return Tag[]|Builder[]|Collection|QueryBuilder[]|SupCollection
     */
    public function getPopular(int $limit = 0)
    {
        if ($limit > 0) {
            return Tag::query()->limit($limit)->get();
        }

        return Tag::query()->get();
    }
}
