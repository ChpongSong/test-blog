<?php


namespace App\Services\Posts;


use App\Contracts\posts\PostQueries;
use App\DTO\posts\PostCreateDto;
use App\Models\Post;
use App\Services\BaseService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class PostService extends BaseService implements PostQueries
{

    /**
     * @param int $perPage
     * @return LengthAwarePaginator|Collection
     */
    public function getAll(int $perPage = 0)
    {
        if ($perPage > 0) {
            return Post::query()
                ->with(["authors", "images"])
                ->orderBy("published_at", "desc")
                ->where("is_published", 1)
                ->paginate($perPage);
        }
        return Post::query()
            ->with(["authors", "images"])
            ->orderBy("published_at", "desc")
            ->where("is_published", 1)
            ->get();
    }


    public function getById($id): Post
    {
        // TODO: Implement getById() method.
    }

    public function getLastPosts()
    {
        return Post::query()->orderBy("published_at", "desc")
            ->where("is_published", 1)->limit(5)->get();
    }

    public function getTopPosts()
    {
        // TODO: Implement getTopPosts() method.
    }

    public function getUserPosts(int $userId)
    {
        // TODO: Implement getUserPosts() method.
    }

    /**
     * @param PostCreateDto $postDto
     * @param $tags
     * @param $images
     * @return bool
     */
    public function create(PostCreateDto $postDto, $tags, $images): bool
    {
        if (
        $post = Post::query()->create([
            "author_id" => $postDto->getAuthorId(),
            "user_id" => $postDto->getUserId(),
            "title" => $postDto->getTitle(),
            "description" => $postDto->getDescription(),
            "post_slug" => $postDto->getSlug(),
            "published_at" => $postDto->getPublishedAt(),
            "is_published" => $postDto->getIsPublished()
        ])
        ) {

            $post->tags()->attach($tags);

            if ($images) {
                foreach ($images as $image) {
                    $fileName = Str::random("20") . '.' . $image->getClientOriginalExtension();
                    $image->storeAs('public/images', $fileName);
                    $post->images()->create([
                        'name' => $fileName,
                        'imageable_id' => $post->id,
                        'imageable_type' => get_class($post)
                    ]);
                }
            }

            return true;
        }
        return false;
    }

}
