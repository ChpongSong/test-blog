<?php


namespace App\Services\Posts;


use App\Models\Author;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection as SupCollection;

class AuthorService extends BaseService
{
    /**
     * @param int $limit
     * @return Author[]|Builder[]|Collection|QueryBuilder[]|SupCollection
     */
    public function getAll(int $limit = 0)
    {
        if ($limit > 0) {
            return Author::query()->limit($limit)->get();
        }
        return Author::query()->get();
    }
}
