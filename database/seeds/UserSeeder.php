<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        foreach (range(1, 100) as $i) {
            $users[] = [
                "email" => $faker->email,
                "name" => $faker->name,
                "password" => bcrypt("test12345")
            ];
        }
        if (!empty($users)) {
            User::query()->insert($users);
        }
    }
}
