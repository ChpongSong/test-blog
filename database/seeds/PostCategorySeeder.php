<?php

use App\Models\PostCategory;
use Illuminate\Database\Seeder;

class PostCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 200) as $i) {
            $postCategories[] = [
                "post_id" => rand(1, 200),
                "category_id" => rand(1, 20)
            ];
        }

        if (!empty($postCategories)) {
            PostCategory::query()->insert($postCategories);
        }
    }
}
