<?php

use App\Models\Category;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        foreach (range(1, 20) as $i) {
            $categoryName = $faker->realText(10);
            $categories[] = [
                "name" => $categoryName,
                "category_slug" => Str::slug($categoryName),
                "parent_id" => $i < 6 ? 0 : rand(1, 5)
            ];
        }

        if (!empty($categories)) {
            Category::query()->insert($categories);
        }
    }
}
