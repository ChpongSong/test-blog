<?php

use App\Models\PostTags;
use Illuminate\Database\Seeder;

class PostTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 50) as $i) {
            $postTags[] = [
                "tag_id" => rand(1, 30),
                "post_id" => rand(1, 200)
            ];
        }

        if (!empty($postTags)) {
            PostTags::query()->insert($postTags);
        }
    }
}
