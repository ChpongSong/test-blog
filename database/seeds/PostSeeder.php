<?php

use App\Models\Post;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        foreach (range(1, 200) as $i) {
            $title = $faker->text(50);
            $posts[] = [
                "title" => $title,
                "description" => $faker->realText(rand(500, 1500)),
                "author_id" => rand(1, 20),
                "user_id" => rand(1, 100),
                "post_slug" => Str::slug($title),
                "published_at" => $faker->dateTimeBetween("2020-01-01", "2020-12-30"),
                "is_published" => 1,
            ];
        }
        if (!empty($posts)) {
            Post::query()->insert($posts);
        }
    }
}
