<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        foreach (range(1, 30) as $i) {
            $tags[] = [
                "name" => uniqid()
            ];
        }

        if (!empty($tags)) {
            Tag::query()->insert($tags);
        }
    }
}
