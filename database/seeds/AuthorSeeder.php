<?php

use App\Models\Author;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        foreach (range(1, 20) as $i) {
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;
            $authors[] = [
                "first_name" => $firstName,
                "last_name" => $lastName,
                "author_slug" => Str::slug("$lastName $firstName", "-"),
            ];
        }

        if (!empty($authors)) {
            Author::query()->insert($authors);
        }
    }
}
